package com.sbt.accountmanagement.webservice.controllers;

import com.sbt.accountmanagement.core.dto.AcceptedOperation;
import com.sbt.accountmanagement.core.dto.Operation;
import com.sbt.accountmanagement.core.ecxeptions.NoSuchPaymentAccountException;
import com.sbt.accountmanagement.webservice.dto.ResultList;
import com.sbt.accountmanagement.webservice.services.impl.AccountService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AccountManagementControllerTest {

    public static final String ACCOUNT_NUMBER = "123456";

    private AccountManagementController accountManagementController;

    @Mock
    private AccountService accountService;

    @BeforeAll
    void beforeAll() {
        MockitoAnnotations.initMocks(this);
        accountManagementController = new AccountManagementController(accountService);
    }

    @Test
    @DisplayName("Получение баланса")
    void getCurrencyAccountBalance() throws NoSuchPaymentAccountException {
        BigDecimal balance = BigDecimal.ONE;
        when(accountService.getCurrencyAccountBalance(anyString()))
                .thenReturn(balance);
        assertEquals(balance, accountManagementController.getCurrencyAccountBalance(ACCOUNT_NUMBER).getData());
    }

    @Test
    @DisplayName("Получить N последних операций")
    void getOperationsByCount() {
        when(accountService.getOperationsByCount(anyString(), anyInt(), anyInt(), anyInt()))
                .thenReturn(new ResultList<>(Collections.singletonList(
                        new AcceptedOperation(Operation.getDebitOperation(ACCOUNT_NUMBER, BigDecimal.ONE))), false));
        assertEquals(1, accountManagementController.
                getOperationsByCount(ACCOUNT_NUMBER, 100, 1, 20).getData().size());
    }

    @Test
    @DisplayName("Получить операции за указанное время")
    void getOperationsForTimePeriod() {
        when(accountService.getOperationsForTimePeriod(anyString(), anyLong(), anyInt(), anyInt()))
                .thenReturn(new ResultList<>(Collections.singletonList(
                        new AcceptedOperation(Operation.getDebitOperation(ACCOUNT_NUMBER, BigDecimal.ONE))), false));
        assertEquals(1, accountManagementController.
                getOperationsForTimePeriod(ACCOUNT_NUMBER, 100L, 1, 20).getData().size());
    }

    @Test
    @DisplayName("Заблокировать счет")
    void lockAccount() throws NoSuchPaymentAccountException {
        when(accountService.lockAccount(anyString()))
                .thenReturn(Boolean.TRUE);
        assertEquals(Boolean.TRUE, accountManagementController.lockAccount(ACCOUNT_NUMBER).getData());
    }

    @Test
    @DisplayName("Разблокировать счет")
    void unlockAccount() throws NoSuchPaymentAccountException {
        when(accountService.unlockAccount(anyString()))
                .thenReturn(Boolean.TRUE);
        assertEquals(Boolean.TRUE, accountManagementController.unlockAccount(ACCOUNT_NUMBER).getData());
    }
}