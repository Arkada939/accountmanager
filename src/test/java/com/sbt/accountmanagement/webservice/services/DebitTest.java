package com.sbt.accountmanagement.webservice.services;

import com.sbt.accountmanagement.core.ecxeptions.BaseAccountManagementException;
import com.sbt.accountmanagement.core.json.JsonManager;
import com.sbt.accountmanagement.core.operationCollections.IncomingOperationsQueue;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DebitTest {

    private Debit debit;

    @Mock
    private IncomingOperationsQueue incomingOperationsQueue;

    @BeforeAll
    void beforeAll() {
        MockitoAnnotations.initMocks(this);
        debit = new Debit(new JsonManager(), incomingOperationsQueue);
    }

    @Test
    @DisplayName("Отправить списания зачисления")
    void sendDebitOperation() throws BaseAccountManagementException {
        String operationId = "123";
        when(incomingOperationsQueue.addOperation(anyString())).thenReturn(operationId);
        assertEquals(operationId, debit.sendDebitOperation("234", BigDecimal.ONE));
    }

    @Test
    @DisplayName("Отправить списания зачисления. Ошибка")
    void sendDebitOperationError() throws BaseAccountManagementException {
        when(incomingOperationsQueue.addOperation(anyString())).thenReturn(null);
        assertThrows(BaseAccountManagementException.class, () -> debit.sendDebitOperation("234", BigDecimal.ONE));
    }
}