package com.sbt.accountmanagement.webservice.services;

import com.sbt.accountmanagement.core.dto.AcceptedOperation;
import com.sbt.accountmanagement.core.dto.Operation;
import com.sbt.accountmanagement.core.ecxeptions.NoSuchPaymentAccountException;
import com.sbt.accountmanagement.core.operationCollections.CompletedOperationMap;
import com.sbt.accountmanagement.core.service.AccountManager;
import com.sbt.accountmanagement.webservice.dto.ResultList;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AccountTest {

    public static final String ACCOUNT_NUMBER = "123456";
    public static final long CURRENT_TIME_MILLIS = System.currentTimeMillis();

    private Account account;

    @Mock
    private AccountManager accountManager;
    @Mock
    private CompletedOperationMap completedOperationMap;

    @BeforeAll
    void beforeAll() {
        MockitoAnnotations.initMocks(this);
        account = new Account(accountManager, completedOperationMap);
    }

    @Test
    @DisplayName("Получить текущий баланс")
    void getCurrencyAccountBalance() throws NoSuchPaymentAccountException {
        BigDecimal balance = BigDecimal.ONE;
        when(accountManager.getCurrencyAccountBalance(anyString())).thenReturn(balance);
        assertEquals(balance, account.getCurrencyAccountBalance(ACCOUNT_NUMBER));
    }

    @Test
    @DisplayName("Поулчить последние N операций, страница 1")
    void getOperationsByCountFirstPage() {
        when(completedOperationMap.getOperationsByAccountNumber(ACCOUNT_NUMBER))
                .thenReturn(createOperationSet());
        ResultList<List<AcceptedOperation>> resultList = account.getOperationsByCount(ACCOUNT_NUMBER, 16, 1, 10);
        assertTrue(resultList.isHasMoreElements());
        assertEquals(10, resultList.getData().size());
        assertEquals(CURRENT_TIME_MILLIS, resultList.getData().get(0).getPaymentDate().getTime());
    }

    @Test
    @DisplayName("Поулчить последние N операций, страница 2")
    void getOperationsByCountSecondPage() {
        when(completedOperationMap.getOperationsByAccountNumber(ACCOUNT_NUMBER))
                .thenReturn(createOperationSet());
        ResultList<List<AcceptedOperation>> resultList = account.getOperationsByCount(ACCOUNT_NUMBER, 16, 2, 10);
        assertFalse(resultList.isHasMoreElements());
        assertEquals(6, resultList.getData().size());
        assertEquals(CURRENT_TIME_MILLIS - 15 * 10000, resultList.getData().get(5).getPaymentDate().getTime());
    }

    @Test
    @DisplayName("Поулчить последние N операций, Пустой ответ")
    void getOperationsByCountEmptyList() {
        when(completedOperationMap.getOperationsByAccountNumber(ACCOUNT_NUMBER))
                .thenReturn(createOperationSet());
        ResultList<List<AcceptedOperation>> resultList = account.getOperationsByCount(ACCOUNT_NUMBER, 16, 3, 10);
        assertFalse(resultList.isHasMoreElements());
        assertEquals(0, resultList.getData().size());
    }

    @Test
    @DisplayName("Получить последние операции за указанное время. Страница 1")
    void getOperationsForTimePeriodFirstPage() {
        when(completedOperationMap.getOperationsByAccountNumber(ACCOUNT_NUMBER))
                .thenReturn(createOperationSet());
        ResultList<List<AcceptedOperation>> resultList = account.getOperationsForTimePeriod(ACCOUNT_NUMBER, (long) (10000 * 16 + 10), 1, 10);
        assertTrue(resultList.isHasMoreElements());
        assertEquals(10, resultList.getData().size());
        assertEquals(CURRENT_TIME_MILLIS, resultList.getData().get(0).getPaymentDate().getTime());
    }

    @Test
    @DisplayName("Получить последние операции за указанное время. Страница 2")
    void getOperationsForTimePeriodSecondPage() {
        when(completedOperationMap.getOperationsByAccountNumber(ACCOUNT_NUMBER))
                .thenReturn(createOperationSet());
        ResultList<List<AcceptedOperation>> resultList = account.getOperationsForTimePeriod(ACCOUNT_NUMBER, (long) (10000 * 16 + 10), 2, 10);
        assertFalse(resultList.isHasMoreElements());
        assertEquals(6, resultList.getData().size());
        assertEquals(CURRENT_TIME_MILLIS - 15 * 10000, resultList.getData().get(5).getPaymentDate().getTime());
    }

    @Test
    @DisplayName("Получить последние операции за указанное время. Пустой ответ")
    void getOperationsForTimePeriodEmptyList() {
        when(completedOperationMap.getOperationsByAccountNumber(ACCOUNT_NUMBER))
                .thenReturn(createOperationSet());
        ResultList<List<AcceptedOperation>> resultList = account.getOperationsForTimePeriod(ACCOUNT_NUMBER, (long) (10000 * 16 + 10), 3, 10);
        assertFalse(resultList.isHasMoreElements());
        assertEquals(0, resultList.getData().size());
    }

    @Test
    @DisplayName("Заблокировать счет")
    void lockAccount() throws NoSuchPaymentAccountException {
        assertEquals(Boolean.TRUE, account.lockAccount(ACCOUNT_NUMBER));
    }

    @Test
    @DisplayName("Разблокировать счет")
    void unlockAccount() throws NoSuchPaymentAccountException {
        assertEquals(Boolean.TRUE, account.unlockAccount(ACCOUNT_NUMBER));
    }

    private TreeSet<AcceptedOperation> createOperationSet(){
        Comparator<AcceptedOperation> comparator = Comparator.comparingLong(operation -> operation.getPaymentDate().getTime());
        TreeSet<AcceptedOperation> operations = new TreeSet<>(comparator.reversed());
        for (int i = 0; i < 20; i++) {
            AcceptedOperation acceptedOperation = new AcceptedOperation(
                    Operation.getDebitOperation(ACCOUNT_NUMBER, BigDecimal.ONE));
            acceptedOperation.setPaymentDate(new Date(CURRENT_TIME_MILLIS - i * 10000));
            operations.add(acceptedOperation);
        }
        return operations;
    }
}