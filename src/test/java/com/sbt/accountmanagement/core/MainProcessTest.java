package com.sbt.accountmanagement.core;

import com.sbt.accountmanagement.core.dto.AcceptedOperation;
import com.sbt.accountmanagement.core.dto.Operation;
import com.sbt.accountmanagement.core.ecxeptions.BaseAccountManagementException;
import com.sbt.accountmanagement.core.ecxeptions.NoSuchPaymentAccountException;
import com.sbt.accountmanagement.core.operationCollections.CompletedOperationMap;
import com.sbt.accountmanagement.core.operationCollections.DebitOperationQueue;
import com.sbt.accountmanagement.core.operationCollections.IncomingOperationsQueue;
import com.sbt.accountmanagement.core.service.AccountManager;
import org.junit.jupiter.api.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MainProcessTest {

    private MainProcess mainProcess;

    @Mock
    private AccountManager accountManager;
    @Mock
    private IncomingOperationsQueue incomingOperationsQueue;
    @Mock
    private DebitOperationQueue debitOperationQueue;
    @Mock
    private CompletedOperationMap completedOperationMap;

    @BeforeAll
    void beforeAll() throws NoSuchPaymentAccountException {
        MockitoAnnotations.initMocks(this);
        mainProcess = new MainProcess(accountManager, incomingOperationsQueue, debitOperationQueue, completedOperationMap);
        when(accountManager.getCurrencyAccountBalance("123")).thenReturn(BigDecimal.ONE);
    }

    @AfterEach
    void afterEach() {
        Mockito.reset(incomingOperationsQueue);
        Mockito.reset(completedOperationMap);
    }

    @Test
    @DisplayName("Обработка операции зачисления. Очередь пустая")
    void processingOperationsCredit() throws BaseAccountManagementException {
        when(incomingOperationsQueue.isEmpty()).thenReturn(Boolean.FALSE).thenReturn(Boolean.TRUE);
        when(incomingOperationsQueue.peek()).thenReturn(new AcceptedOperation(
                Operation.getCreditOperation("123", BigDecimal.ONE)));
        when(accountManager.performCreditOperation(any(AcceptedOperation.class))).thenReturn(Boolean.TRUE);
        when(debitOperationQueue.isEmpty(anyString())).thenReturn(Boolean.TRUE);
        mainProcess.processingOperations();
        verify(incomingOperationsQueue, times(2)).isEmpty();
        verify(completedOperationMap, times(1)).saveCompletedOperation(any(AcceptedOperation.class));
    }

    @Test
    @DisplayName("Обработка операции зачисления. В очереди есть элемент")
    void processingOperationsCreditAndQueue() throws BaseAccountManagementException {
        when(incomingOperationsQueue.isEmpty()).thenReturn(Boolean.FALSE).thenReturn(Boolean.TRUE);
        when(incomingOperationsQueue.peek()).thenReturn(new AcceptedOperation(
                Operation.getCreditOperation("123", BigDecimal.ONE)));
        when(accountManager.performCreditOperation(any(AcceptedOperation.class))).thenReturn(Boolean.TRUE);
        when(debitOperationQueue.isEmpty(anyString())).thenReturn(Boolean.FALSE).thenReturn(Boolean.TRUE);
        when(debitOperationQueue.peek(anyString()))
                .thenReturn(new AcceptedOperation(Operation.getDebitOperation("123", BigDecimal.ONE)));
        when(accountManager.performDebitOperation(any(AcceptedOperation.class))).thenReturn(Boolean.TRUE);
        mainProcess.processingOperations();
        verify(incomingOperationsQueue, times(2)).isEmpty();
        verify(completedOperationMap, times(2)).saveCompletedOperation(any(AcceptedOperation.class));
    }

    @Test
    @DisplayName("Обработка операции зачисления. Отказ из очереди")
    void processingOperationsCreditAndQueueReject() throws BaseAccountManagementException {
        when(incomingOperationsQueue.isEmpty()).thenReturn(Boolean.FALSE).thenReturn(Boolean.TRUE);
        when(incomingOperationsQueue.peek()).thenReturn(new AcceptedOperation(
                Operation.getCreditOperation("123", BigDecimal.ONE)));
        when(accountManager.performCreditOperation(any(AcceptedOperation.class))).thenReturn(Boolean.TRUE);
        when(debitOperationQueue.isEmpty(anyString())).thenReturn(Boolean.FALSE).thenReturn(Boolean.TRUE);
        AcceptedOperation acceptedOperation = new AcceptedOperation(Operation.getDebitOperation("123", BigDecimal.ONE));
        acceptedOperation.setReceiptDate(new Date(System.currentTimeMillis() - 100000));
        when(debitOperationQueue.peek(anyString()))
                .thenReturn(acceptedOperation);
        when(accountManager.performDebitOperation(any(AcceptedOperation.class))).thenReturn(Boolean.TRUE);
        mainProcess.processingOperations();
        verify(incomingOperationsQueue, times(2)).isEmpty();
        verify(completedOperationMap, times(1)).saveCompletedOperation(any(AcceptedOperation.class));
    }

    @Test
    @DisplayName("Обработка операции зачисления. Есть в очереди, но нет средств")
    void processingOperationsCreditAndQueueBalance() throws BaseAccountManagementException {
        when(incomingOperationsQueue.isEmpty()).thenReturn(Boolean.FALSE).thenReturn(Boolean.TRUE);
        when(incomingOperationsQueue.peek()).thenReturn(new AcceptedOperation(
                Operation.getCreditOperation("123", BigDecimal.ONE)));
        when(accountManager.performCreditOperation(any(AcceptedOperation.class))).thenReturn(Boolean.TRUE);
        when(debitOperationQueue.isEmpty(anyString())).thenReturn(Boolean.FALSE).thenReturn(Boolean.TRUE);
        when(debitOperationQueue.peek(anyString()))
                .thenReturn(new AcceptedOperation(Operation.getDebitOperation("123", BigDecimal.ONE)));
        when(accountManager.performDebitOperation(any(AcceptedOperation.class))).thenReturn(Boolean.FALSE);
        mainProcess.processingOperations();
        verify(incomingOperationsQueue, times(2)).isEmpty();
        verify(completedOperationMap, times(1)).saveCompletedOperation(any(AcceptedOperation.class));
    }

    @Test
    @DisplayName("Обработка операции списания. Успешное списание")
    void processingOperationsDebit() throws BaseAccountManagementException {
        when(incomingOperationsQueue.isEmpty()).thenReturn(Boolean.FALSE).thenReturn(Boolean.TRUE);
        when(incomingOperationsQueue.peek()).thenReturn(new AcceptedOperation(
                Operation.getDebitOperation("123", BigDecimal.ONE)));
        when(accountManager.performDebitOperation(any(AcceptedOperation.class))).thenReturn(Boolean.TRUE);
        mainProcess.processingOperations();
        verify(incomingOperationsQueue, times(2)).isEmpty();
        verify(completedOperationMap, times(1)).saveCompletedOperation(any(AcceptedOperation.class));
    }

    @Test
    @DisplayName("Обработка операции списания. Поместить в очередь")
    void processingOperationsDebitQueue() throws BaseAccountManagementException {
        when(incomingOperationsQueue.isEmpty()).thenReturn(Boolean.FALSE).thenReturn(Boolean.TRUE);
        when(incomingOperationsQueue.peek()).thenReturn(new AcceptedOperation(
                Operation.getDebitOperation("123", BigDecimal.ONE)));
        when(accountManager.performDebitOperation(any(AcceptedOperation.class))).thenReturn(Boolean.FALSE);
        when(debitOperationQueue.putOperationToDebitQueue(any(AcceptedOperation.class))).thenReturn(Boolean.TRUE);
        mainProcess.processingOperations();
        verify(incomingOperationsQueue, times(2)).isEmpty();
        verify(debitOperationQueue, times(1)).putOperationToDebitQueue(any(AcceptedOperation.class));
    }
}