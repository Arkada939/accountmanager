package com.sbt.accountmanagement.core.json;

import com.sbt.accountmanagement.core.dto.Operation;
import com.sbt.accountmanagement.core.ecxeptions.BaseAccountManagementException;
import com.sbt.accountmanagement.webservice.dto.ResponseDto;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class JsonManagerTest {

    private final static String FILE_NAME = "debit_credit_operation_schema";

    private JsonManager jsonManager = new JsonManager();

    @Test
    @DisplayName("Проверка обекта по json схеме. Успешно")
    void checkJsonValid() throws BaseAccountManagementException {
        Operation operation = Operation.getDebitOperation("123", BigDecimal.ONE);
        String json = jsonManager.writeValueAsString(operation);

        assertTrue(jsonManager.checkJson(json, FILE_NAME));
    }

    @Test
    @DisplayName("Проверка обекта по json схеме. Неуспешно")
    void checkJsonInvalid() throws BaseAccountManagementException {
        ResponseDto responseDto = new ResponseDto("data");
        String json = jsonManager.writeValueAsString(responseDto);
        assertFalse(jsonManager.checkJson(json, FILE_NAME));
    }

    @Test
    @DisplayName("Проверка обекта по json схеме. Ошибка, файл не сущестует")
    void checkJsonError() throws BaseAccountManagementException {
        ResponseDto responseDto = new ResponseDto("data");
        String json = jsonManager.writeValueAsString(responseDto);
        assertFalse(jsonManager.checkJson(json, "file"));
    }

    @Test
    void writeValueAsString() throws BaseAccountManagementException {
        Operation operation = Operation.getDebitOperation("123", BigDecimal.ONE);

        String json = jsonManager.writeValueAsString(operation);
        assertNotNull(json);
    }

    @Test
    void readValue() throws BaseAccountManagementException {
        Operation operation = Operation.getDebitOperation("123", BigDecimal.ONE);
        String json = jsonManager.writeValueAsString(operation);

        Operation value = jsonManager.readValue(json, Operation.class);
        assertNotNull(value);
    }
}