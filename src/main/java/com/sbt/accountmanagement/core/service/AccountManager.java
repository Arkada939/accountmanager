package com.sbt.accountmanagement.core.service;

import com.sbt.accountmanagement.core.dto.AcceptedOperation;
import com.sbt.accountmanagement.core.dto.PaymentAccount;
import com.sbt.accountmanagement.core.ecxeptions.BaseAccountManagementException;
import com.sbt.accountmanagement.core.ecxeptions.NoSuchPaymentAccountException;
import com.sbt.accountmanagement.core.ecxeptions.OperationTypeException;
import com.sbt.accountmanagement.core.enums.DebitCreditIndicator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class AccountManager {

    protected static final Logger LOGGER = LoggerFactory.getLogger(AccountManager.class);

    private Map<String, PaymentAccount> accountCollection = new ConcurrentHashMap<>();

    public void createNewAccount(String accountNumber) {
        accountCollection.put(accountNumber, new PaymentAccount(accountNumber));
        LOGGER.info("Создан новый счет номер {}", accountNumber);
    }

    /**
     * Зачисление средств на счет
     *
     * @param operation операция зачисления
     * @return при успешном зачислении вернется true, в ином случае будет исключение
     * @throws BaseAccountManagementException
     */
    public boolean performCreditOperation(AcceptedOperation operation) throws BaseAccountManagementException {
        PaymentAccount account = checkAndGetPaymentAccount(operation, DebitCreditIndicator.CREDIT);
        return account.fundsFixIn(operation.getAmount());
    }

    /**
     * Списание средст со счета
     *
     * @param operation операция списания
     * @return true - успешное списание, false - недостаточно средств
     * @throws BaseAccountManagementException
     */
    public boolean performDebitOperation(AcceptedOperation operation) throws BaseAccountManagementException {
        PaymentAccount account = checkAndGetPaymentAccount(operation, DebitCreditIndicator.DEBIT);
        return account.fundsFixOut(operation.getAmount());
    }

    /**
     * Получить текущий баланс счета
     */
    public BigDecimal getCurrencyAccountBalance(String accountNumber) throws NoSuchPaymentAccountException {
        return Optional.ofNullable(accountCollection.get(accountNumber))
                .map(PaymentAccount::getBalance)
                .orElseThrow(() -> new NoSuchPaymentAccountException(accountNumber));
    }

    /**
     * Заблокировать счет
     */
    public void lockAccount(String accountNumber) throws NoSuchPaymentAccountException {
        if (!accountCollection.containsKey(accountNumber)) {
            throw new NoSuchPaymentAccountException(accountNumber);
        }
        accountCollection.get(accountNumber).blockAccount();
    }

    /**
     * Разблокировать счет
     */
    public void unlockAccount(String accountNumber) throws NoSuchPaymentAccountException {
        if (!accountCollection.containsKey(accountNumber)) {
            throw new NoSuchPaymentAccountException(accountNumber);
        }
        accountCollection.get(accountNumber).unlockAccount();
    }

    private PaymentAccount checkAndGetPaymentAccount(AcceptedOperation operation, DebitCreditIndicator expectedIndicator)
            throws BaseAccountManagementException {
        if (expectedIndicator != operation.getDebitCreditIndicator()) {
            throw new OperationTypeException(operation, expectedIndicator);
        }
        PaymentAccount account = accountCollection.get(operation.getAccountNumber());
        if (account == null) {
            throw new NoSuchPaymentAccountException(operation);
        }
        return account;
    }
}
