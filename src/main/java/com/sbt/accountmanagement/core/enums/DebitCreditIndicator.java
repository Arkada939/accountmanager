package com.sbt.accountmanagement.core.enums;

public enum DebitCreditIndicator {
    DEBIT,
    CREDIT;
}
