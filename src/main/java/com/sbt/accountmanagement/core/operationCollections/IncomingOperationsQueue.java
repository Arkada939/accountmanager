package com.sbt.accountmanagement.core.operationCollections;

import com.sbt.accountmanagement.core.dto.AcceptedOperation;
import com.sbt.accountmanagement.core.dto.Operation;
import com.sbt.accountmanagement.core.ecxeptions.BaseAccountManagementException;
import com.sbt.accountmanagement.core.json.JsonManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

@Component
public class IncomingOperationsQueue {

    protected static final Logger LOGGER = LoggerFactory.getLogger(IncomingOperationsQueue.class);

    private final static String SCHEMA_NAME = "debit_credit_operation_schema";

    /**
     * Список поступающих на исполнение операций
     */
    private Queue<AcceptedOperation> incomingOperations = new ConcurrentLinkedQueue<>();


    private JsonManager jsonManager;

    @Autowired
    public IncomingOperationsQueue(JsonManager jsonManager) {
        this.jsonManager = jsonManager;
    }

    /**
     * Проверка операции по схеме и добавление в список операций на выполнение
     *
     * @param jsonOperation операция в виде json
     * @return id в случае успешной валидации операции, null - некорректная операция
     */
    public String addOperation(String jsonOperation) throws BaseAccountManagementException {
        if (jsonManager.checkJson(jsonOperation, SCHEMA_NAME)) {
            Operation operation = jsonManager.readValue(jsonOperation, Operation.class);
            AcceptedOperation acceptedOperation = new AcceptedOperation(operation);
            incomingOperations.add(acceptedOperation);
            LOGGER.info("На обработку добавлена операция: {}", jsonManager.writeValueAsString(acceptedOperation));
            return acceptedOperation.getId();
        }
        return null;
    }

    public AcceptedOperation peek() {
        return incomingOperations.peek();
    }

    public boolean remove(AcceptedOperation operation) {
        return incomingOperations.remove(operation);
    }

    public boolean isEmpty() {
        return incomingOperations.isEmpty();
    }
}
