package com.sbt.accountmanagement.core.operationCollections;

import com.sbt.accountmanagement.core.dto.AcceptedOperation;
import com.sbt.accountmanagement.core.enums.DebitCreditIndicator;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

@Component
public class DebitOperationQueue {

    /**
     * Map для очердей операций списания средств.
     * Ключ - номер счета
     */
    private Map<String, Queue<AcceptedOperation>> debitOperationQueue = new ConcurrentHashMap<>();

    /**
     * Помещает операцию списания в очередь и удаляет из списка поступающих
     *
     * @param acceptedOperation операция списания
     * @return
     */
    public boolean putOperationToDebitQueue(AcceptedOperation acceptedOperation) {
        if (DebitCreditIndicator.DEBIT != acceptedOperation.getDebitCreditIndicator()) {
            return false;
        }
        String accountNumber = acceptedOperation.getAccountNumber();
        if (!debitOperationQueue.containsKey(acceptedOperation.getAccountNumber())) {
            debitOperationQueue.put(accountNumber, new ConcurrentLinkedQueue<>());
        }
        return debitOperationQueue.get(accountNumber).add(acceptedOperation);
    }

    public AcceptedOperation peek(String accountNumber) {
        if (!debitOperationQueue.containsKey(accountNumber)) {
            return null;
        }
        return debitOperationQueue.get(accountNumber).peek();
    }

    public boolean remove(AcceptedOperation acceptedOperation) {
        if (!debitOperationQueue.containsKey(acceptedOperation.getAccountNumber())) {
            //Считаем успешным, так как в очереди нет данной операции
            return true;
        }
        return debitOperationQueue.get(acceptedOperation.getAccountNumber()).remove(acceptedOperation);
    }

    public boolean isEmpty(String accountNumber) {
        if (!debitOperationQueue.containsKey(accountNumber)) {
            return true;
        }
        return debitOperationQueue.get(accountNumber).isEmpty();
    }
}
