package com.sbt.accountmanagement.core.operationCollections;

import com.sbt.accountmanagement.core.dto.AcceptedOperation;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class CompletedOperationMap {

    /**
     * Выполненные операции
     * Ключ - номер счета
     */
    private Map<String, TreeSet<AcceptedOperation>> completedOperation = new ConcurrentHashMap<>();

    /**
     * Сохранить выполненную операцию
     *
     * @param acceptedOperation
     */
    public void saveCompletedOperation(AcceptedOperation acceptedOperation) {
        String accountNumber = acceptedOperation.getAccountNumber();
        if (!completedOperation.containsKey(accountNumber)) {
            initCompletedOperationsSet(accountNumber);
        }
        completedOperation.get(accountNumber).add(acceptedOperation);
    }

    /**
     * Получить список всех операций по счету
     *
     * @param accountNumber номер счета
     * @return список операций
     */
    public TreeSet<AcceptedOperation> getOperationsByAccountNumber(String accountNumber) {
        return completedOperation.get(accountNumber);
    }

    private void initCompletedOperationsSet(String accountNumber) {
        Comparator<AcceptedOperation> comparator = Comparator.comparingLong(operation -> operation.getPaymentDate().getTime());
        TreeSet<AcceptedOperation> operations = new TreeSet<>(comparator.reversed());
        completedOperation.put(accountNumber, operations);
    }
}
