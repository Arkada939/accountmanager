package com.sbt.accountmanagement.core.ecxeptions;

import com.sbt.accountmanagement.core.dto.AcceptedOperation;

public class NoSuchPaymentAccountException extends BaseAccountManagementException {

    public NoSuchPaymentAccountException(AcceptedOperation operation) {
        super(String.format("Счет с номером %s не существует. Невозможно выполнить операцию номер %s",
                operation.getAccountNumber(), operation.getId()));
    }

    public  NoSuchPaymentAccountException(String accountNumber){
        super(String.format("Счет с номером %s не существует", accountNumber));
    }
}
