package com.sbt.accountmanagement.core.ecxeptions;

public class BaseAccountManagementException extends Exception {
    public BaseAccountManagementException() {
    }

    public BaseAccountManagementException(String message) {
        super(message);
    }

    public BaseAccountManagementException(String message, Throwable cause) {
        super(message, cause);
    }

    public BaseAccountManagementException(Throwable cause) {
        super(cause);
    }
}
