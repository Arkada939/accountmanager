package com.sbt.accountmanagement.core.ecxeptions;

public class AccountBlockedException extends BaseAccountManagementException {
    public AccountBlockedException(String accountNumber) {
        super(String.format("Счет номер %s заблокирован. Выполнение операций приостановлено.", accountNumber));
    }
}
