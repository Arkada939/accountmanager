package com.sbt.accountmanagement.core.ecxeptions;

import com.sbt.accountmanagement.core.dto.AcceptedOperation;
import com.sbt.accountmanagement.core.enums.DebitCreditIndicator;

public class OperationTypeException extends BaseAccountManagementException {

    public OperationTypeException(AcceptedOperation operation, DebitCreditIndicator exceptedIndicator) {
        super(String.format("Неверный тип операции id: %s. Ожидается тип - %s" ,
                operation.getId(), exceptedIndicator.name()));
    }
}
