package com.sbt.accountmanagement.core.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.sbt.accountmanagement.core.ecxeptions.BaseAccountManagementException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class JsonManager {

    public static final Logger LOGGER = LoggerFactory.getLogger(JsonManager.class);
    private final static ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private final static JsonSchemaFactory JSON_SCHEMA_FACTORY = JsonSchemaFactory.byDefault();

    private final static String JSON_FILE_TYPE = ".json";
    private final static String FILE_PATH = "src/main/resources/jsonSchemas/";
    private final static String FILE_NAME = "debit_credit_operation_schema";

    public String writeValueAsString(Object value) throws BaseAccountManagementException {
        try {
            return OBJECT_MAPPER.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            LOGGER.error("Ошибка при сериализации объекта: {}", value, e);
            throw new BaseAccountManagementException(e);
        }
    }

    public <T> T readValue(String value, Class<T> clazz) throws BaseAccountManagementException {
        try {
            return OBJECT_MAPPER.readValue(value, clazz);
        } catch (JsonProcessingException e) {
            LOGGER.error("Ошибка при десериализации объекта: {}", value, e);
            throw new BaseAccountManagementException(e);
        }
    }

    public boolean checkJson(String json, String schemaName) {
        try {
            JsonNode jsonSchemaNode = JsonLoader.fromPath(FILE_PATH + addJsonTypeToFileName(schemaName));
            JsonSchema jsonSchema = JSON_SCHEMA_FACTORY.getJsonSchema(jsonSchemaNode);
            ProcessingReport report = jsonSchema.validate(JsonLoader.fromString(json));
            if (!report.isSuccess()) {
                for (ProcessingMessage processingMessage : report) {
                    String pointer = processingMessage.asJson().get("instance").get("pointer").asText();
                    LOGGER.warn(pointer + " : " + processingMessage.getMessage());
                }
            }
            return report.isSuccess();
        } catch (ProcessingException | IOException e) {
            LOGGER.error("Ошибка при валидации объекта: {}, json-схема: {}", json, FILE_NAME, e);
            return Boolean.FALSE;
        }
    }

    private String addJsonTypeToFileName(String fileName) {
        return fileName.endsWith(JSON_FILE_TYPE) ? fileName : fileName + JSON_FILE_TYPE;
    }

}
