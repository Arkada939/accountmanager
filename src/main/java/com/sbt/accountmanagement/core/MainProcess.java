package com.sbt.accountmanagement.core;

import com.sbt.accountmanagement.core.dto.AcceptedOperation;
import com.sbt.accountmanagement.core.ecxeptions.BaseAccountManagementException;
import com.sbt.accountmanagement.core.enums.DebitCreditIndicator;
import com.sbt.accountmanagement.core.operationCollections.CompletedOperationMap;
import com.sbt.accountmanagement.core.operationCollections.DebitOperationQueue;
import com.sbt.accountmanagement.core.operationCollections.IncomingOperationsQueue;
import com.sbt.accountmanagement.core.service.AccountManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class MainProcess {

    //Промежуток времени в миллисекундах, после которого отказываем операцию
    public static final Long CUTOFF_TIME = 60000L;

    protected static final Logger LOGGER = LoggerFactory.getLogger(MainProcess.class);

    private AccountManager accountManager;
    private IncomingOperationsQueue incomingOperationsQueue;
    private DebitOperationQueue debitOperationQueue;
    private CompletedOperationMap completedOperationMap;

    @Autowired
    public MainProcess(AccountManager accountManager,
                       IncomingOperationsQueue incomingOperationsQueue,
                       DebitOperationQueue debitOperationQueue,
                       CompletedOperationMap completedOperationMap) {
        this.accountManager = accountManager;
        this.incomingOperationsQueue = incomingOperationsQueue;
        this.debitOperationQueue = debitOperationQueue;
        this.completedOperationMap = completedOperationMap;
    }

    @Scheduled(fixedDelay = 5000)
    public void processingOperations() {
        LOGGER.info("Начал работу процесс обработки операций");
        while (!incomingOperationsQueue.isEmpty()) {
            AcceptedOperation acceptedOperation = incomingOperationsQueue.peek();
            operationProcessing(acceptedOperation);
        }
        LOGGER.info("Приостановлена работа процесса обработки операций, все операции обработаны");
    }

    private void operationProcessing(AcceptedOperation acceptedOperation) {
        try {
            boolean executionResult;
            if (DebitCreditIndicator.CREDIT == acceptedOperation.getDebitCreditIndicator()) {
                executionResult = creditOperationExecution(acceptedOperation);
            } else {
                executionResult = debitOperationExecution(acceptedOperation);
            }
            if (Boolean.TRUE.equals(executionResult)) {
                acceptedOperation.markOperationCompleted();
                completedOperationMap.saveCompletedOperation(acceptedOperation);
                incomingOperationsQueue.remove(acceptedOperation);
            }
        } catch (BaseAccountManagementException e) {
            LOGGER.error("Ошибка при выполнении операции номер {}", acceptedOperation.getId(), e);
        }
    }

    /**
     * Выполнение операции зачисления.
     * В случае успешного зачисления смотрим очередь операций списания
     *
     * @param acceptedOperation операция зачисления
     * @return результат зачисления
     * @throws BaseAccountManagementException
     */
    private boolean creditOperationExecution(AcceptedOperation acceptedOperation) throws BaseAccountManagementException {
        boolean isCreditOperationCompleted = accountManager.performCreditOperation(acceptedOperation);
        if (isCreditOperationCompleted) {
            LOGGER.info("Операция зачисления id = {} выполнена, Баланс = {}", acceptedOperation.getId(),
                    accountManager.getCurrencyAccountBalance(acceptedOperation.getAccountNumber()));
            checkDebitQueue(acceptedOperation.getAccountNumber());
        }
        return isCreditOperationCompleted;
    }

    private void checkDebitQueue(String accountNumber) throws BaseAccountManagementException {
        //Выполняем, пока в очереди есть операции
        while (!debitOperationQueue.isEmpty(accountNumber)) {
            AcceptedOperation acceptedOperation = debitOperationQueue.peek(accountNumber);
            if (acceptedOperation != null) {
                if (System.currentTimeMillis() - acceptedOperation.getReceiptDate().getTime() > CUTOFF_TIME) {
                    debitOperationQueue.remove(acceptedOperation);
                    LOGGER.info("Операция списания id = {} отказана, превышено время ожидания", acceptedOperation.getId());
                    continue;
                }
                boolean isDebitOperationCompleted = accountManager.performDebitOperation(acceptedOperation);
                if (isDebitOperationCompleted) {
                    completedOperationMap.saveCompletedOperation(acceptedOperation);
                    debitOperationQueue.remove(acceptedOperation);
                    LOGGER.info("Операция списания id = {} выполнена и удалена из очереди ожидания, Баланс = {}", acceptedOperation.getId(),
                            accountManager.getCurrencyAccountBalance(accountNumber));
                } else {
                    //если на счете недостаточно средств завершаем выполнение операций из очереди
                    break;
                }
            }
        }
    }

    private boolean debitOperationExecution(AcceptedOperation acceptedOperation) throws BaseAccountManagementException {
        boolean isDebitOperationCompleted = accountManager.performDebitOperation(acceptedOperation);
        boolean putToQueue = false;
        if (!isDebitOperationCompleted) {
            putToQueue = debitOperationQueue.putOperationToDebitQueue(acceptedOperation);
            LOGGER.info("Операция списаня id = {} помещена в очередь ожидания средств", acceptedOperation.getId());
        } else {
            LOGGER.info("Операция списания id = {} выполнена, Баланс = {}", acceptedOperation.getId(),
                    accountManager.getCurrencyAccountBalance(acceptedOperation.getAccountNumber()));
        }
        return isDebitOperationCompleted || putToQueue;
    }

}
