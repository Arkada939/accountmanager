package com.sbt.accountmanagement.core.dto;

import java.util.Date;

public class AcceptedOperation extends Operation {

    private String id;
    private Date receiptDate;
    private Date paymentDate;
    private Long threadId;

    public AcceptedOperation(Operation operation) {
        super(operation);
        receiptDate = new Date();
        id = String.valueOf(Math.round(Math.random() * 1000000000));
        threadId = Thread.currentThread().getId();
    }

    /**
     * Пометить операцю выполненной
     * Задать дату выполнения операции
     */
    public void markOperationCompleted() {
        this.paymentDate = new Date();
    }

    public String getId() {
        return id;
    }

    public Date getReceiptDate() {
        return receiptDate;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public Long getThreadId() {
        return threadId;
    }

    /**
     * Сеттеры только для тестов
     */
    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public void setReceiptDate(Date receiptDate) {
        this.receiptDate = receiptDate;
    }
}
