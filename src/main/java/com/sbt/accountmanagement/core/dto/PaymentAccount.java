package com.sbt.accountmanagement.core.dto;

import com.sbt.accountmanagement.core.ecxeptions.AccountBlockedException;

import java.math.BigDecimal;

/**
 * Расчетный счет
 */
public class PaymentAccount {

    private String number;
    private BigDecimal balance;
    private boolean isBlocked;

    public PaymentAccount(String number) {
        this.number = number;
        balance = BigDecimal.ZERO;
        isBlocked = Boolean.FALSE;
    }

    /**
     * Зачисление средств на счет
     *
     * @param amount сумма к причислению
     * @return true в случае успешного зачисления
     * @throws AccountBlockedException если счет заблокирован
     */
    public boolean fundsFixIn(BigDecimal amount) throws AccountBlockedException {
        if (isBlocked) {
            throw new AccountBlockedException(number);
        }
        balance = balance.add(amount);
        return true;
    }

    /**
     * Снятие средств со счета
     *
     * @param amount сумма списания
     * @return true в случае успешного списания, false - если недостаточно средств
     * @throws AccountBlockedException если счет заблокирован
     */
    public boolean fundsFixOut(BigDecimal amount) throws AccountBlockedException {
        if (isBlocked) {
            throw new AccountBlockedException(number);
        }
        if (balance.compareTo(amount) < 0) {
            return false;
        }
        balance = balance.subtract(amount);
        return true;
    }

    /**
     * Заблокировать счет
     */
    public void blockAccount() {
        this.isBlocked = Boolean.TRUE;
    }

    /**
     * Разблокировать счет
     */
    public void unlockAccount() {
        this.isBlocked = Boolean.FALSE;
    }

    public String getNumber() {
        return number;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public boolean isBlocked() {
        return isBlocked;
    }
}
