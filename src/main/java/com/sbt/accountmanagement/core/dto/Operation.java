package com.sbt.accountmanagement.core.dto;

import com.sbt.accountmanagement.core.enums.DebitCreditIndicator;

import java.io.Serializable;
import java.math.BigDecimal;

public class Operation implements Serializable {

    private String accountNumber;
    private DebitCreditIndicator debitCreditIndicator;
    private BigDecimal amount;

    public Operation() {
    }

    private Operation(String accountNumber, DebitCreditIndicator debitCreditIndicator, BigDecimal amount) {
        this.accountNumber = accountNumber;
        this.debitCreditIndicator = debitCreditIndicator;
        this.amount = amount;
    }

    protected Operation(Operation operation) {
        this.accountNumber = operation.accountNumber;
        this.debitCreditIndicator = operation.debitCreditIndicator;
        this.amount = operation.amount;
    }

    /**
     * Создать операцию спасания
     *
     * @param accountNumber номер счета
     * @param amount        сумма списания
     * @return операция списания
     */
    public static Operation getDebitOperation(String accountNumber, BigDecimal amount) {
        return new Operation(accountNumber, DebitCreditIndicator.DEBIT, amount);
    }

    /**
     * Создать операию зачисления
     *
     * @param accountNumber номер счета
     * @param amount        сумма зачисления
     * @return операция зачисления
     */
    public static Operation getCreditOperation(String accountNumber, BigDecimal amount) {
        return new Operation(accountNumber, DebitCreditIndicator.CREDIT, amount);
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public DebitCreditIndicator getDebitCreditIndicator() {
        return debitCreditIndicator;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
