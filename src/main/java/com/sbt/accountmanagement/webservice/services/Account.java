package com.sbt.accountmanagement.webservice.services;

import com.sbt.accountmanagement.core.dto.AcceptedOperation;
import com.sbt.accountmanagement.core.ecxeptions.NoSuchPaymentAccountException;
import com.sbt.accountmanagement.core.operationCollections.CompletedOperationMap;
import com.sbt.accountmanagement.core.service.AccountManager;
import com.sbt.accountmanagement.webservice.dto.ResultList;
import com.sbt.accountmanagement.webservice.services.impl.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeSet;

@Service
public class Account implements AccountService {

    private AccountManager accountManager;
    private CompletedOperationMap completedOperationMap;

    @Autowired
    public Account(AccountManager accountManager, CompletedOperationMap completedOperationMap) {
        this.accountManager = accountManager;
        this.completedOperationMap = completedOperationMap;
    }

    @Override
    public BigDecimal getCurrencyAccountBalance(String accountNumber) throws NoSuchPaymentAccountException {
        return accountManager.getCurrencyAccountBalance(accountNumber);
    }

    @Override
    public ResultList<List<AcceptedOperation>> getOperationsByCount(String accountNumber, int count, int pageNumber, int pageSize) {
        TreeSet<AcceptedOperation> operations = completedOperationMap.getOperationsByAccountNumber(accountNumber);

        int toElement = pageNumber * pageSize;
        int fromElement = toElement - pageSize + 1;
        int leftToLoad = count - (fromElement - 1);

        if (operations == null || fromElement > operations.size() || leftToLoad < 1) {
            return new ResultList<>(Collections.emptyList(), false);
        }
        List<AcceptedOperation> result = new ArrayList<>();
        int currentElement = 1;
        for (AcceptedOperation operation : operations) {
            if (result.size() < leftToLoad && result.size() < pageSize) {
                if (currentElement >= fromElement) {
                    result.add(operation);
                }
            } else {
                break;
            }
            currentElement++;
        }
        return new ResultList<>(result, leftToLoad > pageSize);
    }

    @Override
    public ResultList<List<AcceptedOperation>> getOperationsForTimePeriod(String accountNumber, Long time, int pageNumber, int pageSize) {
        TreeSet<AcceptedOperation> operations = completedOperationMap.getOperationsByAccountNumber(accountNumber);

        int toElement = pageNumber * pageSize;
        int fromElement = toElement - pageSize + 1;

        if (operations == null || fromElement > operations.size()) {
            return new ResultList<>(Collections.emptyList(), false);
        }

        long currentTime = System.currentTimeMillis();
        int currentElement = 1;

        List<AcceptedOperation> result = new ArrayList<>();
        boolean hasMoreElement = false;

        for (AcceptedOperation operation : operations) {
            if (currentTime - operation.getPaymentDate().getTime() < time) {
                if (currentElement >= fromElement && currentElement <= toElement) {
                    result.add(operation);
                } else if (currentElement > toElement) {
                    hasMoreElement = true;
                    break;
                }
            } else {
                break;
            }
            currentElement++;
        }
        return new ResultList<>(result, hasMoreElement);
    }

    @Override
    public Boolean lockAccount(String accountNumber) throws NoSuchPaymentAccountException {
        accountManager.lockAccount(accountNumber);
        return true;
    }

    @Override
    public Boolean unlockAccount(String accountNumber) throws NoSuchPaymentAccountException {
        accountManager.unlockAccount(accountNumber);
        return true;
    }
}
