package com.sbt.accountmanagement.webservice.services.impl;

import com.sbt.accountmanagement.core.ecxeptions.BaseAccountManagementException;

import java.math.BigDecimal;

/**
 * Сервис для операций по списанию средств
 */
public interface DebitService {

    /**
     * Отправить операцию списания
     *
     * @param accountNumber номер счета
     * @param amount        сумма списания
     * @return id операции списания
     * @throws BaseAccountManagementException
     */
    String sendDebitOperation(String accountNumber, BigDecimal amount) throws BaseAccountManagementException;
}
