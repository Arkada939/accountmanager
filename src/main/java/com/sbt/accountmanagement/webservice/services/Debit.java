package com.sbt.accountmanagement.webservice.services;

import com.sbt.accountmanagement.core.dto.Operation;
import com.sbt.accountmanagement.core.ecxeptions.BaseAccountManagementException;
import com.sbt.accountmanagement.core.json.JsonManager;
import com.sbt.accountmanagement.core.operationCollections.IncomingOperationsQueue;
import com.sbt.accountmanagement.webservice.services.impl.DebitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class Debit implements DebitService {

    private JsonManager jsonManager;
    private IncomingOperationsQueue incomingOperationsQueue;

    @Autowired
    public Debit(JsonManager jsonManager,
                 IncomingOperationsQueue incomingOperationsQueue) {
        this.jsonManager = jsonManager;
        this.incomingOperationsQueue = incomingOperationsQueue;
    }

    public String sendDebitOperation(String accountNumber, BigDecimal amount) throws BaseAccountManagementException {
        Operation operation = Operation.getDebitOperation(accountNumber, amount);
        String jsonOperation = jsonManager.writeValueAsString(operation);
        String operationId = incomingOperationsQueue.addOperation(jsonOperation);
        if (operationId == null) {
            throw new BaseAccountManagementException("Некорректная операция");
        }
        return operationId;
    }

}
