package com.sbt.accountmanagement.webservice.services.impl;

import com.sbt.accountmanagement.core.ecxeptions.BaseAccountManagementException;

import java.math.BigDecimal;

/**
 * Сервис для работы с операциями зачисления
 */
public interface CreditService {

    /**
     * Отправить операцию зачисления
     *
     * @param accountNumber номер счета
     * @param amount        сумма к причислению
     * @return id отправенной операции
     */
    String sendCreditOperation(String accountNumber, BigDecimal amount) throws BaseAccountManagementException;
}
