package com.sbt.accountmanagement.webservice.services;

import com.sbt.accountmanagement.core.dto.Operation;
import com.sbt.accountmanagement.core.ecxeptions.BaseAccountManagementException;
import com.sbt.accountmanagement.core.json.JsonManager;
import com.sbt.accountmanagement.core.operationCollections.IncomingOperationsQueue;
import com.sbt.accountmanagement.webservice.services.impl.CreditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class Credit implements CreditService {

    private JsonManager jsonManager;
    private IncomingOperationsQueue incomingOperationsQueue;

    @Autowired
    public Credit(JsonManager jsonManager,
                  IncomingOperationsQueue incomingOperationsQueue) {
        this.jsonManager = jsonManager;
        this.incomingOperationsQueue = incomingOperationsQueue;
    }

    public String sendCreditOperation(String accountNumber, BigDecimal amount) throws BaseAccountManagementException {
        Operation operation = Operation.getCreditOperation(accountNumber, amount);
        String jsonOperation = jsonManager.writeValueAsString(operation);
        String operationId = incomingOperationsQueue.addOperation(jsonOperation);
        if (operationId == null) {
            throw new BaseAccountManagementException("Некорректная операция");
        }
        return operationId;
    }
}
