package com.sbt.accountmanagement.webservice.services.impl;

import com.sbt.accountmanagement.core.dto.AcceptedOperation;
import com.sbt.accountmanagement.core.ecxeptions.NoSuchPaymentAccountException;
import com.sbt.accountmanagement.webservice.dto.ResultList;

import java.math.BigDecimal;
import java.util.List;

/**
 * Сервис для взаимодействия со счетами
 */
public interface AccountService {

    /**
     * Получить текущий баланс счета
     *
     * @param accountNumber
     * @return
     * @throws NoSuchPaymentAccountException
     */
    BigDecimal getCurrencyAccountBalance(String accountNumber) throws NoSuchPaymentAccountException;

    /**
     * Получить указанное количество выполненных операций по счету
     *
     * @param accountNumber
     * @param count
     * @return
     */
    ResultList<List<AcceptedOperation>> getOperationsByCount(String accountNumber, int count, int pageNumber, int pageSize);

    /**
     * Получить список операций по счету, выполненных за указанное время
     *
     * @param accountNumber
     * @param time
     * @return
     */
    ResultList<List<AcceptedOperation>> getOperationsForTimePeriod(String accountNumber, Long time, int pageNumber, int pageSize);

    /**
     * Заблокировать счет
     *
     * @param accountNumber
     * @return
     * @throws NoSuchPaymentAccountException
     */
    Boolean lockAccount(String accountNumber) throws NoSuchPaymentAccountException;

    /**
     * Разблокировать счет
     *
     * @param accountNumber
     * @return
     * @throws NoSuchPaymentAccountException
     */
    Boolean unlockAccount(String accountNumber) throws NoSuchPaymentAccountException;
}
