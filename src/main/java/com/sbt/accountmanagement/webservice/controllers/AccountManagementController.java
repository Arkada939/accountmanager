package com.sbt.accountmanagement.webservice.controllers;

import com.sbt.accountmanagement.core.dto.AcceptedOperation;
import com.sbt.accountmanagement.webservice.dto.ResponseDto;
import com.sbt.accountmanagement.webservice.services.impl.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;

@RestController
public class AccountManagementController extends BaseController {

    protected static final Logger LOGGER = LoggerFactory.getLogger(AccountManagementController.class);

    private AccountService accountService;

    @Autowired
    public AccountManagementController(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * Текущий остаток на счете
     */
    @RequestMapping("/getCurrencyAccountBalance")
    public ResponseDto<BigDecimal> getCurrencyAccountBalance(String accountNumber) {
        return sendResponse(() -> accountService.getCurrencyAccountBalance(accountNumber));
    }

    /**
     * Список последних N операций
     */
    @RequestMapping("/getOperationsByCount")
    public ResponseDto<List<AcceptedOperation>> getOperationsByCount(String accountNumber, int count, int pageNumber, int pageSize) {
        return sendListResponse(() -> accountService.getOperationsByCount(accountNumber, count, pageNumber, pageSize));
    }

    /**
     * Список всех операций за указанный период времени (в миллисекундах) вплоть до текущего момента
     */
    @RequestMapping("/getOperationsForTimePeriod")
    public ResponseDto<List<AcceptedOperation>> getOperationsForTimePeriod(String accountNumber, Long time, int pageNumber, int pageSize) {
        return sendListResponse(() -> accountService.getOperationsForTimePeriod(accountNumber, time, pageNumber, pageSize));
    }

    /**
     * Блокировка счета (все операции с момента блокировки отклоняются)
     */
    @RequestMapping("/lockAccount")
    public ResponseDto<Boolean> lockAccount(String accountNumber) {
        return sendResponse(() -> accountService.lockAccount(accountNumber));
    }

    /**
     * Разблокировка счета (все операции снова осуществляются)
     */
    @RequestMapping("/unlockAccount")
    public ResponseDto<Boolean> unlockAccount(String accountNumber) {
        return sendResponse(() -> accountService.unlockAccount(accountNumber));
    }

}
