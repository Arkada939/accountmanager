package com.sbt.accountmanagement.webservice.controllers;

import com.sbt.accountmanagement.webservice.dto.ResponseDto;
import com.sbt.accountmanagement.webservice.dto.ResultList;
import com.sbt.accountmanagement.webservice.enums.ResponseStatus;
import com.sbt.accountmanagement.webservice.function.ThrowingSupplier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;

public abstract class BaseController {

    protected static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    protected <T> ResponseDto<T> sendResponse(ThrowingSupplier<T> supplier) {
        ResponseDto<T> responseDto;
        try {
            responseDto = new ResponseDto<>(supplier.get());
        } catch (Exception e) {
            responseDto = getErrorResponse(e);
        }
        return responseDto;
    }

    protected <T> ResponseDto<T> sendListResponse(ThrowingSupplier<ResultList<T>> supplier) {
        ResponseDto<T> responseDto;
        try {
            responseDto = new ResponseDto<>(supplier.get());
        } catch (Exception e) {
            responseDto = getErrorResponse(e);
        }
        return responseDto;
    }

    private <T> ResponseDto<T> getErrorResponse(Exception e) {
        LOGGER.error("Error", e);
        return new ResponseDto<T>(ResponseStatus.ERROR, e.getMessage());
    }
}
