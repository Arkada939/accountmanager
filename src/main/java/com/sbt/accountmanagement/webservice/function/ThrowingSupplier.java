package com.sbt.accountmanagement.webservice.function;

@FunctionalInterface
public interface ThrowingSupplier<T> {
    T get() throws Exception;
}
