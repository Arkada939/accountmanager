package com.sbt.accountmanagement.webservice.enums;

public enum ResponseStatus {
    SUCCESSFUL,
    ERROR;
}
