package com.sbt.accountmanagement.webservice.dto;

import com.sbt.accountmanagement.webservice.enums.ResponseStatus;

public class ResponseDto<T> {
    /**
     * Статус выполнения ответа
     */
    private String status;
    /**
     * Сообщение ошибки
     */
    private String errorMessage;
    /**
     * Данные ответа
     */
    private T data;

    /**
     * Есть ли еще елементы для постраничного получения
     */
    private boolean hasMoreElements = false;

    public ResponseDto() {
        status = ResponseStatus.SUCCESSFUL.name();
    }

    public ResponseDto(T data) {
        status = ResponseStatus.SUCCESSFUL.name();
        this.data = data;
    }

    public ResponseDto(ResponseStatus status, String errorMessage) {
        this.status = status.name();
        this.errorMessage = errorMessage;
    }

    public ResponseDto(ResultList<T> resultList) {
        data = resultList.getData();
        hasMoreElements = resultList.isHasMoreElements();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
