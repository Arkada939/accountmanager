package com.sbt.accountmanagement.webservice.dto;

public class ResultList<T> {

    private T data;
    private boolean hasMoreElements = false;


    public ResultList(T data, boolean hasMoreElements) {
        this.data = data;
        this.hasMoreElements = hasMoreElements;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public boolean isHasMoreElements() {
        return hasMoreElements;
    }

    public void setHasMoreElements(boolean hasMoreElements) {
        this.hasMoreElements = hasMoreElements;
    }
}
