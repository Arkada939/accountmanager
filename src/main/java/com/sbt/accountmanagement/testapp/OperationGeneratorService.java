package com.sbt.accountmanagement.testapp;

import com.sbt.accountmanagement.core.ecxeptions.BaseAccountManagementException;
import com.sbt.accountmanagement.webservice.services.impl.CreditService;
import com.sbt.accountmanagement.webservice.services.impl.DebitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * Класс генерации тестовых потоков для формирования операций
 */
@Service
public class OperationGeneratorService {

    public static final String ACCOUNT_NUMBER = "1234567890";
    public static final Integer OPERATION_COUNT = 10;
    public static final Double MAX_AMOUNT = 100D;

    private DebitService debitService;
    private CreditService creditService;

    @Autowired
    public OperationGeneratorService(DebitService debitService, CreditService creditService) {
        this.debitService = debitService;
        this.creditService = creditService;
    }

    @Async
    public void createThread() throws BaseAccountManagementException, InterruptedException {
        for (int i = 0; i < OPERATION_COUNT; i++) {

            Thread.sleep((long) (Math.random() * 5000));

            BigDecimal amount = new BigDecimal(Math.random() * MAX_AMOUNT).setScale(2, BigDecimal.ROUND_HALF_EVEN);
            if (Math.random() > 0.5) {
                debitService.sendDebitOperation(ACCOUNT_NUMBER, amount);
            } else {
                creditService.sendCreditOperation(ACCOUNT_NUMBER, amount);
            }
        }
    }
}
