package com.sbt.accountmanagement.testapp;

import com.sbt.accountmanagement.core.ecxeptions.BaseAccountManagementException;
import com.sbt.accountmanagement.core.service.AccountManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * Главные тестовый сервис, создает один счет и запускает тестовые потоки
 * Запускается после инициализации Spring приложения.
 * Запуск приложения выполняется из класса AccountManagementApplication.class
 */
@Service
public class TestService {

    public static final String ACCOUNT_NUMBER = "1234567890";
    public static final Integer COUNT_OF_THREADS = 10;

    private OperationGeneratorService operationGeneratorService;
    private AccountManager accountManager;

    @Autowired
    public TestService(AccountManager accountManager, OperationGeneratorService operationGeneratorService) {
        this.operationGeneratorService = operationGeneratorService;
        this.accountManager = accountManager;
    }

    @PostConstruct
    public void runTest() throws BaseAccountManagementException, InterruptedException {
        accountManager.createNewAccount(ACCOUNT_NUMBER);
        for (int i = 0; i < COUNT_OF_THREADS; i++) {
            operationGeneratorService.createThread();
        }
    }


}
